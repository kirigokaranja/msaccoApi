from routes import app, db

from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from database.user import User
from database.member import Member
from database.guarantor import Guarantor
from database.loanApplication import LoanApplication
from database.loan import Loan
from database.notification import Notification
from database.resetPassword import ResetPassword
from database.statement import Statement
from database.repayment import Repayment
from database.interest import Interest

migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
	manager.run()

