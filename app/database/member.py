from datetime import datetime

from routes import db
from database.timestamp import SharedColumns

class Member(db.Model, SharedColumns):
    __tablename__ = "members"
    member_id = db.Column(db.Integer, primary_key = True)
    member_public_id = db.Column(db.String(100), unique = True, nullable = False, index = True)
    user_public_id = db.Column(db.String(100), db.ForeignKey("users.user_public_id"), index = True, nullable = False)
    firstname = db.Column(db.String(100), nullable = False)
    lastname = db.Column(db.String(100), nullable = False)
    gender = db.Column(db.String(100), nullable = False)
    credit_score = db.Column(db.Integer, nullable = False)
    annual_income = db.Column(db.Integer)
    credit_problems = db.Column(db.Integer, nullable = False)
    tax_liens = db.Column(db.Integer)
    years_job = db.Column(db.Integer)
    years_history = db.Column(db.Integer, nullable = False)
    IDNo = db.Column(db.String(100), nullable = False)
    member_shares = db.Column(db.String(100), nullable = False)
    telephone = db.Column(db.String(100))
    home_ownership = db.Column(db.String(100))

    #Relationships
    members = db.relationship("LoanApplication", backref = "rel_application_member", lazy = "dynamic")
    guarantors = db.relationship("Guarantor", backref="rel_member_details", lazy="dynamic")

    def __init__(self, member_public_id, user_public_id, firstname, lastname,
                 gender,credit_score,annual_income,credit_problems,tax_liens,years_history,
                 IDNo, member_shares, telephone, home_ownership, years_job, created_at,updated_at):
        self.member_public_id = member_public_id
        self.user_public_id = user_public_id
        self.firstname = firstname
        self.lastname = lastname
        self.gender = gender
        self.credit_score = credit_score
        self.annual_income = annual_income
        self.credit_problems = credit_problems
        self.tax_liens = tax_liens
        self.years_history = years_history
        self.IDNo = IDNo
        self.member_shares = member_shares
        self.telephone = telephone
        self.home_ownership = home_ownership
        self.years_job = years_job
        self.created_at = created_at
        self.updated_at = updated_at

    def __repr__(self):
        return "<Member {}, {}, {}, {},{}, {}, {}, {}, {},>".format(self.member_public_id, self.user_public_id, self.firstname,self.lastname, self.IDNo, self.member_shares, self.telephone, self.home_ownership, self.years_job)

    def __str__(self):
        return "<Member {}, {}, {}, {},{}, {}, {}, {}, {},>".format(self.member_public_id, self.user_public_id, self.firstname,self.lastname, self.IDNo, self.member_shares, self.telephone, self.home_ownership, self.years_job)

    def return_json(self):
        return{
            "member_id": self.member_id,
            "member_public_id": self.member_public_id,
            "user_public_id": self.user_public_id,
            "member_firstname": self.firstname,
            "member_lastname": self.lastname,
            "member_names": self.firstname + " " +self.lastname,
            "id_no": self.IDNo,
            "gender": self.gender,
            "credit_score": self.credit_score,
            "annual_income":self.annual_income,
            "credit_problems": self.credit_problems,
            "tax_liens": self.tax_liens,
            "years_job": self.years_job,
            "years_history": self.years_history,
            "member_shares": self.member_shares,
            "telephone": self.telephone,
            "home_ownership": self.home_ownership,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
        }
            