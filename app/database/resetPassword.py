from routes import db
from database.timestamp import SharedColumns

class ResetPassword(db.Model, SharedColumns):
    __tablename__ = "reset_password"
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), nullable=False)
    token = db.Column(db.Text, nullable=False)
    reset_public_id = db.Column(db.String(100), nullable=False, index=True, unique=True)
    reset = db.Column(db.String(100), nullable=False)

    def __init__(self, email, token, reset_public_id, reset, created_at, updated_at):
        self.email = email
        self.token = token
        self.reset_public_id = reset_public_id
        self.reset = reset
        self.created_at = created_at
        self.updated_at = updated_at

    def __repr__(self):
        return "<ResetPassword {}, {}, {}, {]>".format(self.email, self.token, self.reset_public_id, self.reset)

    def __str__(self):
        return "<ResetPassword {}, {}, {}, {]>".format(self.email, self.token, self.reset_public_id, self.reset)

    def return_json(self):
        return {
            "reset_public_id": self.reset_public_id,
            "email": self.email,
            "token": self.token,
            "reset": self.reset,
            "created_at": self.created_at,
            "updated_at": self.updated_at
        }