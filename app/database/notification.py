from datetime import datetime

from routes import db
from database.timestamp import SharedColumns

class Notification(db.Model, SharedColumns):
    __tablename__ = "notifications"
    notification_id = db.Column(db.Integer, primary_key = True)
    notification_public_id = db.Column(db.String(100), unique = True, nullable = False, index = True)
    message = db.Column(db.String(100), nullable = False)
    recipient_id = db.Column(db.String(100), db.ForeignKey("users.user_public_id"), index = True, nullable = False)
    notification_status = db.Column(db.String(100), nullable = False)
    notification_read = db.Column(db.String(100), nullable = False)
    notification_date = db.Column(db.DateTime)

    def __init__(self, notification_public_id, message, recipient_id, notification_status, notification_read, notification_date, created_at,updated_at):
        self.notification_public_id = notification_public_id
        self.message = message
        self.recipient_id = recipient_id
        self.notification_status = notification_status
        self.notification_read = notification_read
        self.notification_date = notification_date
        self.created_at = created_at
        self.updated_at = updated_at

    def __repr__(self):
        return "<Notification {}, {}, {}, {}, {}, {}>".format(self.notification_public_id, self.message, self.recipient_id, 
        self.notification_status, self.notification_read, self.notification_date)

    def __str__(self):
        return "<Notification {}, {}, {}, {}, {}, {}>".format(self.notification_public_id, self.message, self.recipient_id, 
        self.notification_status, self.notification_read, self.notification_date)

    def return_json(self):
        return{
            "notification_public_id": self.notification_public_id,
            "message": self.message,
            "recipient_id": self.recipient_id,
            "notification_status": self.notification_status,
            "notification_read": self.notification_read,
            "notification_date": self.notification_date,
            "created_at": self.created_at,
            "updated_at": self.updated_at
        }