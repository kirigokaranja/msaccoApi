from datetime import datetime
from passlib.hash import bcrypt
import jwt

from routes import db, app
from database.timestamp import SharedColumns

class User(db.Model, SharedColumns):
    __tablename__ = "users"
    user_id = db.Column(db.Integer, primary_key = True)
    user_public_id = db.Column(db.String(100), unique = True, nullable = False, index = True)
    user_email = db.Column(db.String(100), unique = True, nullable = False, index = True)
    firstname = db.Column(db.String(100), nullable=False)
    lastname = db.Column(db.String(100), nullable=False)
    user_password = db.Column(db.String(100), nullable = False)
    user_status = db.Column(db.String(100), nullable = False)
    user_category = db.Column(db.String(100), nullable = False)

    # Relationships
    user_details = db.relationship("Member", backref = "rel_member_details", lazy = "dynamic")
    #user_notifications = db.relationship("Notification", backref = "rel_user_notifications", lazy = "dynamic")
    guarantors = db.relationship("Guarantor", backref="rel_application_user_guarantors", lazy="dynamic")

    def hash_password(self, password):
        self.user_password = bcrypt.hash(password)

    def verify_password(self, password, ):
        if bcrypt.verify(password, self.user_password):
            return True
        else:
            return False

    def encode_auth_token(self, user_id):
        try:
            payload = {
                'exp': datetime.utcnow() + datetime.timedelta(days=1, seconds=5),
                'iat': datetime.utcnow(),
                'sub': user_id
            }
            return jwt.encode(
                payload,
                app.config.get('SECRET_KEY'),
                algorithm='HS256'
            )
        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(auth_token):
        """
        Validates the auth token
        :param auth_token:
        :return: integer|string
        """
        try:
            payload = jwt.decode(auth_token, app.config['SECRET_KEY'], 'utf-8')
            is_blacklisted_token = BlacklistToken.check_blacklist(auth_token)
            if is_blacklisted_token:
                return 'Token blacklisted. Please log in again.'
            else:
                return payload['sub']
        except jwt.ExpiredSignatureError:
            return 'Signature expired. Please log in again'
        except jwt.InvalidTokenError:
            return 'Invalid token. Please log in again.'

    def __init__(self, user_public_id = None, user_email = None,
    user_password = None, user_status = None, user_category = None,firstname = None, lastname = None, created_at = None,updated_at = None):
        self.user_public_id = user_public_id
        self.user_email = user_email
        self.firstname = firstname
        self.lastname = lastname
        self.user_password = user_password
        self.user_status = user_status
        self.user_category = user_category
        self.created_at = created_at
        self.updated_at = updated_at
    
    def __str__(self):
        return "<User {}, {}, {}, {}>",format(self.user_public_id,self.user_email,
        self.user_status,self.user_category)

    def __repr__(self):
        return "<User {}, {}, {}, {}>",format(self.user_public_id,self.user_email,
        self.user_status,self.user_category)

    def return_json(self):
        return{
            "user_public_id": self.user_public_id,
            "user_email": self.user_email,
            "user_names": self.firstname + " " +self.lastname,
            "user_status": self.user_status,
            "user_category": self.user_category,
            "personal_details": [
                detail.return_json() for detail in self.user_details
            ],
            "created_at": self.created_at,
            "updated_at": self.updated_at,
        }

class BlacklistToken(db.Model):
    """
    Token Model for storing JWT tokens
    """
    __tablename__ = 'blacklist_tokens'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    token = db.Column(db.String(500), nullable=False)
    blacklisted_on = db.Column(db.DateTime, nullable=False)

    def __init__(self, token):
        self.token = token
        self.blacklisted_on = datetime.now()

    def __repr__(self):
        return '<id: token: {}'.format(self.token)

    @staticmethod
    def check_blacklist(auth_token):
        # check whether auth token has been blacklisted
        res = BlacklistToken.query.filter_by(token=str(auth_token)).first()
        if res:
            return True
        else:
            return False