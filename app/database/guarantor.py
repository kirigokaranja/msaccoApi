from datetime import datetime

from routes import db
from database.timestamp import SharedColumns

class Guarantor(db.Model, SharedColumns):
    __tablename__ = "guarantor"
    guarantor_id = db.Column(db.Integer, primary_key = True)
    guarantor_public_id = db.Column(db.String(100), unique = True, nullable = False, index = True)
    user_public_id = db.Column(db.String(100), db.ForeignKey("users.user_public_id"), index = True, nullable = False)
    member_public_id = db.Column(db.String(100), db.ForeignKey("members.member_public_id"), index=True)
    loan_application_public_id = db.Column(db.String(100), db.ForeignKey("loan_application.loan_application_public_id"), index = True, nullable = False)
    amount_guaranteed = db.Column(db.String(100), nullable = False)
    other_loans_guaranteed = db.Column(db.String(100))
    rating = db.Column(db.String(100))
    recommendation = db.Column(db.String(100))

    #Relationships
    
    

    def __init__(self, guarantor_public_id, user_public_id,member_public_id,  loan_application_public_id, amount_guaranteed,other_loans_guaranteed, created_at,updated_at, recommendation, rating=None):
        self.guarantor_public_id = guarantor_public_id
        self.user_public_id = user_public_id
        self.member_public_id = member_public_id
        self.loan_application_public_id = loan_application_public_id
        self.amount_guaranteed = amount_guaranteed
        self.other_loans_guaranteed = other_loans_guaranteed
        self.recommendation = recommendation
        self.rating = rating
        self.created_at = created_at
        self.updated_at = updated_at

    def __repr__(self):
        return "<Guarantor {}, {}, {}, {}, {}>".format(self.guarantor_public_id, self.user_public_id, self.loan_application_public_id, self.amount_guaranteed, self.other_loans_guaranteed)

    def __str__(self):
        return "<Guarantor {}, {}, {}, {}, {}>".format(self.guarantor_public_id, self.user_public_id, self.loan_application_public_id, self.amount_guaranteed, self.other_loans_guaranteed)

    def return_json(self):
        return{
            "guarantor_public_id": self.guarantor_public_id,
            "user_public_id": self.user_public_id,
            "member_public_id": self.member_public_id,
            "loan_application_public_id": self.loan_application_public_id,
            "amount_guaranteed": self.amount_guaranteed,
            "other_loans_guaranteed": self.other_loans_guaranteed,
            "rating": self.rating,
            "recommendation": self.recommendation,
            "loan_application_member": self.rel_application_guarantors.rel_application_member.firstname + " " + self.rel_application_guarantors.rel_application_member.lastname,
            "application_date": self.rel_application_guarantors.application_date,
            "loan_amount_figures": self.rel_application_guarantors.loan_amount_figures,
            "no_of_installements": self.rel_application_guarantors.no_of_installements,
            "monthly_installements_figures": self.rel_application_guarantors.monthly_installements_figures,
            "loan_purpose": self.rel_application_guarantors.loan_purpose,
            "loan_status": self.rel_application_guarantors.loan_status,
            "risk_class": self.rel_application_guarantors.risk_class,
            "reject_reason": self.rel_application_guarantors.reject_reason,
            "created_at": self.created_at,
            "updated_at": self.updated_at
        }

    def return_member_guarantor_details_json(self):
        return{
            "guarantor_public_id": self.guarantor_public_id,
            "user_public_id": self.user_public_id,
            "loan_application_public_id": self.loan_application_public_id,
            "amount_guaranteed": self.amount_guaranteed,
            "recommendation": self.recommendation,
            "member_id": self.rel_member_details.member_id,
            "member_name": self.rel_member_details.firstname + " " + self.rel_member_details.lastname,
            "member_national_id": self.rel_member_details.IDNo,
            "member_shares": self.rel_member_details.member_shares,
            "telephone": self.rel_member_details.telephone,
            "created_at": self.created_at,
            "updated_at": self.updated_at
        }
