from datetime import datetime

from routes import db
from database.timestamp import SharedColumns

class Repayment(db.Model, SharedColumns):
    __tablename__ = "repayments"
    payment_id = db.Column(db.Integer, primary_key = True)
    payment_public_id = db.Column(db.String(100), unique = True, nullable = False, index = True)
    loan_id = db.Column(db.String(100), db.ForeignKey("loans.loan_public_id"), index = True, nullable = False)
    payment_date = db.Column(db.DateTime)
    payment_amount = db.Column(db.String(100), nullable = False)
    payment_type = db.Column(db.String(100), nullable = False)

    def __init__(self, payment_public_id, loan_id, payment_type, payment_amount, payment_date, created_at, updated_at):
        self.payment_public_id = payment_public_id
        self.loan_id = loan_id
        self.payment_type = payment_type
        self.payment_date = payment_date
        self.payment_amount = payment_amount
        self.created_at = created_at
        self.updated_at = updated_at

    def __repr__(self):
        return "<Payment {}, {}, {}, {}, {}>".format(self.payment_public_id, self.loan_id, self.payment_type, self.payment_amount,self.payment_date)

    def __str__(self):
        return "<Payment {}, {}, {}, {}, {}>".format(self.payment_public_id, self.loan_id, self.payment_type, self.payment_amount,self.payment_date)

    def return_json(self):
        return{
            "payment_public_id": self.payment_public_id,
            "loan_id": self.loan_id,
            "payment_type": self.payment_type,
            "payment_amount": self.payment_amount,
            "payment_date": self.payment_date,
            "created_at": self.created_at,
            "updated_at": self.updated_at
        }