from datetime import datetime

from routes import db
from database.timestamp import SharedColumns

class Statement(db.Model, SharedColumns):
    __tablename__ = "statements"
    statement_id = db.Column(db.Integer, primary_key = True)
    statement_public_id = db.Column(db.String(100), unique = True, nullable = False, index = True)
    loan_id = db.Column(db.String(100), db.ForeignKey("loans.loan_public_id"), index = True, nullable = False)
    statement_type = db.Column(db.String(100), nullable = False)
    statement_date = db.Column(db.DateTime)
    debit = db.Column(db.String(100))
    credit = db.Column(db.String(100))

    def __init__(self, statement_public_id, loan_id, statement_type,statement_date, debit, credit, created_at, updated_at):
        self.statement_public_id = statement_public_id
        self.loan_id = loan_id
        self.statement_type = statement_type
        self.statement_date = statement_date
        self.debit = debit
        self.credit = credit
        self.created_at = created_at
        self.updated_at = updated_at

    def __repr__(self):
        return "<Statement {}, {}, {}, {}, {}, {}>".format(self.statement_public_id, self.loan_id, self.statement_date, self.statement_type, self.debit, self.credit)

    def __str__(self):
        return "<Statement {}, {}, {}, {}, {}, {}>".format(self.statement_public_id, self.loan_id, self.statement_date, self.statement_type, self.debit, self.credit)

    def return_json(self):
        return{
            "statement_public_id": self.statement_public_id,
            "loan_id": self.loan_id,
            "statement_type": self.statement_type,
            "statement_date": self.statement_date,
            "credit": self.credit,
            "debit": self.debit,
            "created_at": self.created_at,
            "updated_at": self.updated_at
        }