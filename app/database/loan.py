from datetime import datetime

from routes import db
from database.timestamp import SharedColumns

class Loan(db.Model, SharedColumns):
    __tablename__ = "loans"
    loan_id = db.Column(db.Integer, primary_key = True)
    loan_public_id = db.Column(db.String(100), unique = True, nullable = False, index = True)
    loan_application_public_id = db.Column(db.String(100), db.ForeignKey("loan_application.loan_application_public_id"), index = True, nullable = False)
    approved_date = db.Column(db.DateTime)
    approved_amount = db.Column(db.String(100))
    cheque_no = db.Column(db.String(100))
    cheque_date = db.Column(db.DateTime)
    current_balance = db.Column(db.String(100))
    completion_date = db.Column(db.DateTime)

    statements = db.relationship("Statement", backref="rel_statement_details", lazy="dynamic")

    def __init__(self, loan_application_public_id,loan_public_id,approved_date, approved_amount,  created_at,updated_at, cheque_no = None, cheque_date = None, current_balance = None, completion_date = None):
        self.loan_public_id = loan_public_id
        self.loan_application_public_id = loan_application_public_id
        self.approved_date = approved_date
        self.approved_amount = approved_amount
        self.cheque_no = cheque_no
        self.cheque_date = cheque_date
        self.current_balance = current_balance
        self.completion_date = completion_date
        self.created_at = created_at
        self.updated_at = updated_at

    def __repr__(self):
        return "<Loan {}, {}, {}, {}, {}, {}, {}>".formart(self.loan_public_id, self.loan_application_public_id, self.approved_date, self.cheque_no, self.cheque_date, self.current_balance, self.completion_date)

    def __str__(self):
        return "<Loan {}, {}, {}, {}, {}, {}, {}>".formart(self.loan_public_id, self.loan_application_public_id, self.approved_date, self.cheque_no, self.cheque_date, self.current_balance, self.completion_date)

    def return_json(self):
        return{
            "loan_public_id": self.loan_public_id,
            "loan_application_public_id": self.loan_application_public_id,
            "approved_date": self.approved_date,
            "approved amount": self.approved_amount,
            "cheque_no": self.cheque_no,
            "cheque_date": self.cheque_date,
            "current_balance": self.current_balance,
            "completion_date": self.completion_date,
            "statement_details": [
                detail.return_json() for detail in self.statements
            ],
            "created_at": self.created_at,
            "updated_at": self.updated_at
        }