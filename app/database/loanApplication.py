from datetime import datetime

from routes import db
from database.timestamp import SharedColumns

class LoanApplication(db.Model, SharedColumns):
    __tablename__ = "loan_application"
    loan_application_id = db.Column(db.Integer, primary_key = True)
    loan_application_public_id = db.Column(db.String(100), unique = True, nullable = False, index = True)
    member_public_id = db.Column(db.String(100), db.ForeignKey("members.member_public_id"), index = True, nullable = False)
    loan_amount_figures =  db.Column(db.String(100), nullable = False)
    loan_amount_words =  db.Column(db.String(100), nullable = False)
    no_of_installements =  db.Column(db.String(100), nullable = False)
    monthly_installements_figures =  db.Column(db.String(100), nullable = False)
    monthly_installements_words =  db.Column(db.String(100), nullable = False)
    loan_purpose =  db.Column(db.String(100), nullable = False)
    guarantor_public_id1 =  db.Column(db.String(100), nullable = False)
    guarantor_public_id2 =  db.Column(db.String(100), nullable = False)
    guarantor_public_id3 = db.Column(db.String(100))
    application_date =  db.Column(db.DateTime)
    loan_status =  db.Column(db.String(100), nullable = False)
    risk_class =  db.Column(db.String(100), nullable = False)
    reject_reason = db.Column(db.String(100))

    # Relationships
    loan_application_guarantors = db.relationship("Guarantor", backref = "rel_application_guarantors", lazy = "dynamic")
    # approved_loans = db.relationship("Loan", backref = "rel_approved_loans", lazy = "dynamic")


    def __init__(self, loan_application_public_id, member_public_id, loan_amount_figures, loan_amount_words, no_of_installements, monthly_installements_figures,
    monthly_installements_words, loan_purpose, guarantor_public_id1, guarantor_public_id2, guarantor_public_id3, application_date,
    updated_at,loan_status, risk_class, created_at, reject_reason=None):
        self.loan_application_public_id = loan_application_public_id
        self.member_public_id = member_public_id
        self.loan_amount_figures = loan_amount_figures
        self.loan_amount_words = loan_amount_words
        self.no_of_installements = no_of_installements
        self.monthly_installements_figures = monthly_installements_figures
        self.monthly_installements_words = monthly_installements_words
        self.loan_purpose = loan_purpose
        self.loan_status = loan_status
        self.guarantor_public_id1 = guarantor_public_id1
        self.guarantor_public_id2 = guarantor_public_id2
        self.guarantor_public_id3 = guarantor_public_id3
        self.application_date = application_date
        self.risk_class = risk_class
        self.created_at = created_at
        self.updated_at = updated_at
        self.reject_reason = reject_reason
      
    
    def __repr__(self):
        return "<LoanApplication {}, {}, {}, {}, {}, {}, {}, {},{}, {}, {}, {},{}, {}, {}>"\
        .format(self.loan_application_public_id, self.member_public_id, self.loan_amount_figures, self.loan_amount_words,
         self.no_of_installements,self.monthly_installements_figures, self.monthly_installements_words, self.loan_purpose, 
         self.loan_status, self.risk_class,self.guarantor_public_id1, self.guarantor_public_id2, self.guarantor_public_id3,
          self.application_date, self.reject_reason)

    def __str__(self):
        return "<LoanApplication {}, {}, {}, {}, {}, {}, {}, {},{}, {}, {}, {},{}, {}, {}>"\
        .format(self.loan_application_public_id, self.member_public_id, self.loan_amount_figures, self.loan_amount_words,
         self.no_of_installements,self.monthly_installements_figures, self.monthly_installements_words, self.loan_purpose, 
         self.loan_status, self.risk_class,self.guarantor_public_id1, self.guarantor_public_id2, self.guarantor_public_id3,
          self.application_date, self.reject_reason)

    def return_json(self):
        return {
            "loan_application_public_id" : self.loan_application_public_id,
            "member_public_id" : self.member_public_id,
            "member_id": self.rel_application_member.member_id,
            "member_name": self.rel_application_member.firstname + " " + self.rel_application_member.lastname,
            "member_national_id": self.rel_application_member.IDNo,
            "member_shares": self.rel_application_member.member_shares,
            "application_date": self.application_date,
            "telephone": self.rel_application_member.telephone,
            "home_ownership": self.rel_application_member.home_ownership,
            "loan_amount_figures" : self.loan_amount_figures,
            "loan_amount_words" : self.loan_amount_words,
            "no_of_installements" : self.no_of_installements,
            "monthly_installements_figures" : self.monthly_installements_figures,
            "monthly_installements_words" : self.monthly_installements_words,
            "loan_purpose" : self.loan_purpose,
            "loan_status" : self.loan_status,
            "reject_reason": self.reject_reason,
            "guarantor_public_id1" : self.guarantor_public_id1,
            "guarantor_public_id2" : self.guarantor_public_id2,
            "guarantor_public_id3" : self.guarantor_public_id3,
            "risk_class" : self.risk_class,
            "created_at" : self.created_at,
            "updated_at" : self.updated_at,

        }

    def return_loan_member_json(self):
    
        return {
            "loan_application_public_id" : self.loan_application_public_id,
            "member_public_id" : self.member_public_id,
            "member_id": self.rel_application_member.member_id,
            "member_name": self.rel_application_member.firstname + " " + self.rel_application_member.lastname,
            "member_national_id": self.rel_application_member.IDNo,
            "member_shares": self.rel_application_member.member_shares,
            "application_date": self.application_date,
            "telephone": self.rel_application_member.telephone,
            "home_ownership": self.rel_application_member.home_ownership,
            "loan_amount_figures" : self.loan_amount_figures,
            "loan_amount_words" : self.loan_amount_words,
            "no_of_installements" : self.no_of_installements,
            "monthly_installements_figures" : self.monthly_installements_figures,
            "monthly_installements_words" : self.monthly_installements_words,
            "loan_purpose" : self.loan_purpose,
            "loan_status" : self.loan_status,
            "guarantors":  [
                detail.return_member_guarantor_details_json() for detail in self.loan_application_guarantors
            ],
            # "approved_loan": [
            #     detailed.return_json() for detailed in self.approved_loans
            # ],
            "risk_class" : self.risk_class,
            "reject_reason": self.reject_reason,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
        }


    
