from datetime import datetime

from routes import db
from database.timestamp import SharedColumns

class Interest(db.Model, SharedColumns):
    __tablename__ = "interests"
    interest_id = db.Column(db.Integer, primary_key = True)
    interest_public_id = db.Column(db.String(100), unique = True, nullable = False, index = True)
    loan_id = db.Column(db.String(100), db.ForeignKey("loans.loan_public_id"), index = True, nullable = False)
    interest_date = db.Column(db.DateTime)
    interest_amount = db.Column(db.String(100), nullable = False)

    def __init__(self, interest_public_id, loan_id, interest_date, interest_amount, created_at, updated_at):
        self.interest_public_id = interest_public_id
        self.loan_id = loan_id
        self.interest_date = interest_date
        self.interest_amount = interest_amount
        self.created_at = created_at
        self.updated_at = updated_at

    def __repr__(self):
        return "<Interest {}, {}, {}, {}>".format(self.interest_public_id, self.loan_id, self.interest_amount, self.interest_date)

    def __str__(self):
        return "<Interest {}, {}, {}, {}>".format(self.interest_public_id, self.loan_id, self.interest_amount, self.interest_date)

    def return_json(self):
        return{
            "interest_public_id": self.interest_public_id,
            "loan_id": self.loan_id,
            "interest_date": self.interest_date,
            "interest_amount": self.interest_amount,
            "created_at": self.created_at,
            "updated_at": self.updated_at

        }