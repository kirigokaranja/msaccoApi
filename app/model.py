import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.linear_model import LogisticRegressionCV, SGDClassifier, LogisticRegression
from sklearn.impute import SimpleImputer
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix,accuracy_score,recall_score,classification_report
from sklearn.neural_network import MLPClassifier
from sklearn.externals import joblib

df = pd.read_csv('data.csv')
df.drop('Loan_ID', axis=1, inplace= True)

df.Loan_Status.replace({'Y': 0, 'N': 1}, inplace= True)
df['Loan_Status']= df.Loan_Status.astype(int)
dummies= pd.get_dummies(df, drop_first=True)

SimImp = SimpleImputer()
train= pd.DataFrame(SimImp.fit_transform(dummies), columns=dummies.columns)

train['Loan_Term_360']= np.where(train.Loan_Amount_Term == 360, 1, 0)
train.drop('Loan_Amount_Term', inplace= True, axis= 1)

categorical_cols= [*df.select_dtypes(['Int64', 'Float64']).columns]
categorical_cols.remove('Loan_Amount_Term')
categorical_cols.remove('Credit_History')
train_data = train.drop(categorical_cols, axis=1)
X, y = train_data.drop('Loan_Status', axis=1), train_data.Loan_Status
X_train, X_test, y_train, y_test= train_test_split(X, y, test_size=0.2, random_state=123, stratify= y)

clf = MLPClassifier(solver='lbfgs', alpha=1e-5,hidden_layer_sizes=(5, 2), random_state=1)
clf.fit(X_train, y_train)
sgd_pred= clf.predict(X_test)
print("Accuracy score : ",accuracy_score(y_test, sgd_pred))
print ("Recall score   : ", recall_score(sgd_pred,y_test))
confusion_matrix(y_test, sgd_pred)
print ("classification report :\n",classification_report(sgd_pred,y_test))

joblib.dump(clf, 'model.pkl')
model_columns = list(X.columns)
print(model_columns)
joblib.dump(model_columns, 'model_columns.pkl')