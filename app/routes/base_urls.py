import requests

from flask import jsonify, request
from datetime import datetime

from routes import app, db

@app.route("/", methods = ["GET", "PUT", "PATCH", "POST"])
def index_route():
	"""
	This is the index endpoint
	---
	tags:
		- base_urls.py
	responses:
		200:
			description:
				Everything is running smoothly. We are good to go.
			schema:
				properties:
					message:
						type: object
						example:
							["Muthandi Sacco Application"]
	"""
	message = []
	
	message.append("Muthandi Sacco Application")

	return jsonify({
		"message": message
	}), 200


@app.errorhandler(404)
def page_not_found(e):
    response = []
    response.append("Page was not found")
    print(e)
    return jsonify({'message': response}), 404