import requests
import uuid
import random
import string

from flask import jsonify, request
from datetime import datetime

from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

from routes import app, db
from database.member import Member
from database.user import User
from routes.auth_function import token_required

def generatePassword(stringLength=10):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

@app.route("/member/add", methods = ["POST"])
# @token_required
def add_new_member():

    user_id = str(uuid.uuid4())
    now = datetime.now()
    first_name = request.json.get("first_name")
    last_name = request.json.get("last_name")
    email = request.json.get("email")
    password = generatePassword()

    member = Member(
        member_public_id = str(uuid.uuid4()),
        user_public_id = user_id,
        firstname = first_name,
        lastname = last_name,
        IDNo = request.json.get("id_no"),
        member_shares = request.json.get("member_shares"),
        telephone = request.json.get("telephone"),
        home_ownership = request.json.get("home_ownership"),
        gender = request.json.get("gender"),
        credit_score = request.json.get("credit_score"),
        annual_income = request.json.get("annual_income"),
        credit_problems = request.json.get("credit_problems"),
        tax_liens = request.json.get("tax_liens"),
        years_job = request.json.get("years_job"),
        years_history = request.json.get("years_history"),
        created_at = now,
        updated_at = now
    )
    db.session.add(member)
    user = User(
        user_public_id=user_id,
        user_email=email,
        firstname=first_name,
        lastname = last_name,
        user_status="Active",
        user_category=2,
        created_at = now,
        updated_at = now
    )
    user.hash_password(password)
    db.session.add(user)
    print(password)

    try:
        db.session.commit()
        db.session.close()

        message = Mail(
            from_email=app.config['MAIL_ADDRESS'],
            to_emails=email,
            subject='Msacco Member Registration',
            html_content='<h1>Welcome to Msacco,</h1> <p>You are now a registered member</p> <p>Your password is: {0}</p>'.format(password))

        try:
            sg = SendGridAPIClient(app.config['SENDGRID_API_KEY'])
            response = sg.send(message)
            if response:
                return jsonify({
                    "message": "Member has been added successfully"
                }), 201
        except Exception as e:
            print("error message", e)

    except Exception as e:
        db.session.rollback()
        db.session.close()
        message = "Member has not been added"
        return jsonify({
			"message": message,
			"exception": str(e)
		}), 406

@app.route("/view/members/all", methods = ["GET"])
# @token_required
def view_all_members():
    members = db.session.query(Member).all()

    if not members:
        return jsonify({
            "message": "No members found in the databse"
        }), 412

    else:
        data = []
        for single in members:
            data.append(single.return_json())

        return jsonify({
            "message": "All members fetched successfully",
			"data": data
		}), 200

@app.route("/view/member/<public_id>", methods = ["GET"])
@token_required
def view_single_member(public_id):
	"""
	Return details of a single member
	"""
	member = db.session.query(Member)\
					 .filter(Member.user_public_id == public_id)\
					 .first()

	if not member:

		return jsonify({
			"message": "The selected user does not appear to exist"
		}), 404
	else:
		data = member.return_json()

		return jsonify({
            "message": "Member fetched successfully",
			"data": data
		}), 200

@app.route("/update/member/<public_id>", methods=["POST"])
def update_single_member(public_id):

    member = db.session.query(Member)\
					 .filter(Member.member_public_id == public_id)\
					 .first()
    
    if not member:
        return jsonify({'message' : 'Member Not Found'}), 412
    else:
        firstname = request.json.get("first_name"),
        lastname = request.json.get("last_name"),
        IDNo = request.json.get("id_no"),
        telephone = request.json.get("telephone"),
        home_ownership = request.json.get("home_ownership"),
        member_shares = request.json.get("member_shares")
        gender = request.json.get("gender"),
        credit_score = request.json.get("credit_score"),
        annual_income = request.json.get("annual_income"),
        credit_problems = request.json.get("credit_problems"),
        tax_liens = request.json.get("tax_liens"),
        years_job = request.json.get("years_job"),
        years_history = request.json.get("years_history"),

        member.firstname = firstname
        member.lastname = lastname
        member.telephone = telephone
        member.IDNo = IDNo
        member.home_ownership = home_ownership
        member.member_shares = member_shares
        member.gender = gender
        member.credit_score = credit_score
        member.annual_income = annual_income
        member.credit_problems = credit_problems
        member.tax_liens = tax_liens
        member.years_job = years_job
        member.years_history = years_history
        member.updated_at = datetime.now()


        try:
            db.session.commit()
            db.session.close()

            return jsonify({
                "message": "Member has been updated successfully"
            }), 201

        except Exception as e:
            db.session.rollback()
            db.session.close()
            return jsonify({
                "message": "Member has not been updated",
                "exception": str(e)
            }), 406
