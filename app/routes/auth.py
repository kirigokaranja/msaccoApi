import requests
import uuid
import jwt
import os

from flask import jsonify, request
from datetime import datetime, timedelta
from routes.auth_function import token_required

from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

from routes import app, db
from database.user import User, BlacklistToken
from database.resetPassword import ResetPassword

@app.route("/login", methods = ["POST"])
def login():
	password = request.json.get("password")
	email = request.json.get("email")
	if email is None or password is None:
		return jsonify({
			"message": "missing parameters"
		}), 422
	# user = db.session.query(User).filter(User.user_email == email).first()
	users = User().query.filter(User.user_email == email).first()
	if users and users.verify_password(password):
		jwt_paylod = {
			"iss": "Msacco",
			"sub": users.user_public_id,
			'exp': datetime.utcnow() + timedelta(days=1, seconds=5),
            'iat': datetime.now(),
		}

		access_token = jwt.encode(jwt_paylod, app.config["SECRET_KEY"], algorithm='HS256')

		if access_token:
			return jsonify(
				{
					"message": "Login successful",
					"access_token": access_token.decode("utf-8"),
					"user": users.return_json()
				}), 200
		else:
			return jsonify({
				"message": "No token generated"
			}), 406

	else:
		return jsonify({
			"message": "Incorrect username or password. Please try again"
		}), 406

@app.route("/logout", methods=["GET"])
def logout():
	auth_header = request.headers.get('Authorization')

	if auth_header:
		try:
			auth_token = auth_header.split(" ")[1]
		except IndexError:
			return jsonify({
				"message": "Bearer token malfunctioned"
			}), 401
	else:
		auth_token = ''

	if not auth_token:
		return jsonify({
			"message": "Token is missing"
		}), 401
	try:
		data = jwt.decode(auth_token, app.config['SECRET_KEY'], 'utf-8')
		blacklist_token = BlacklistToken(
			token=auth_token
		)
		db.session.add(blacklist_token)
		try:
			db.session.commit()
			db.session.close()
			message = ["Successfully logged out"]
			return jsonify({
				"message": message
			}), 200
		except Exception as e:
			db.session.rollback()
			db.session.close()
			message = ["Logout error"]
			return jsonify({
				"message": message,
				"exception": str(e)
			}), 406
	except Exception as e:
		return jsonify({
			"message": "Provide a valid auth token",
			"error": str(e)
		}), 401

@app.route("/protected")
@token_required
def protected():
	users = db.session.query(User).all()

	if not users:
		message = ["No user found in the databse"]
		return jsonify({
			"message": message
		}), 412

	else:
		data = []
		for single in users:
			data.append(single.return_json())

		return jsonify({
			"data": data
		}), 200

@app.route("/password/reset", methods=["POST"])
def reset_password():
	email = request.json.get("email")

	user = db.session.query(User) \
		.filter(User.user_email == email) \
		.first()

	if not user:

		return jsonify({
			"message": "The selected user does not appear to exist"
		}), 404
	else:

		jwt_paylod = {
			"iss": "Msacco_ResetPassword",
			"sub": user.user_public_id,
			'exp': datetime.utcnow() + timedelta(days=1, seconds=5),
			'iat': datetime.now(),
		}

		token = jwt.encode(jwt_paylod, app.config["SECRET_KEY"], algorithm='HS256')
		reset_public_id = str(uuid.uuid4())

		find_existence= db.session.query(ResetPassword) \
			.filter(ResetPassword.email == email, ResetPassword.reset == "False") \
			.first()
		if find_existence:
			find_existence.reset_public_id = reset_public_id
			find_existence.token = token
			find_existence.updated_at = datetime.now()

			try:
				db.session.commit()
				db.session.close()

				message = Mail(
					from_email=app.config['MAIL_ADDRESS'],
					to_emails=email,
					subject='Msacco Reset Password Link',
					html_content='<p> Follow this link to reset your Msacco password for your {0} account.</p> <p> http://localhost:8080/resetPassword?resetId={1} </p>'.format(email, reset_public_id))
				try:
					sg = SendGridAPIClient(app.config['SENDGRID_API_KEY'])
					response = sg.send(message)
					if response:
						return jsonify({
							"message": 'Sent an email to {0}. Check the email for instructions.'.format(email)
						}), 200
				except Exception as e:
					print("error message", e.message)

			except Exception as e:
				db.session.rollback()
				db.session.close()
				message = ["Error occured with Password Reset Link"]
				return jsonify({
					"message": message,
					"exception": str(e)
				}), 406

		else:
			reset_password = ResetPassword(
				email=email,
				token=token,
				reset_public_id=reset_public_id,
				reset="False",
				created_at=datetime.now(),
				updated_at=datetime.now()
			)
			db.session.add(reset_password)

			try:
				db.session.commit()
				db.session.close()

				message = Mail(
					from_email=app.config['MAIL_ADDRESS'],
					to_emails=email,
					subject='Msacco Reset Password Link',
					html_content='<p> Follow this link to reset your Msacco password for your {0} account.</p> <p> http://localhost:8080/resetPassword?resetId={1} </p>'.format(email, reset_public_id))
				try:
					sg = SendGridAPIClient(app.config['SENDGRID_API_KEY'])
					response = sg.send(message)
					if response:
						return jsonify({
							"message": 'Sent an email to {0}. Check the email for instructions.'.format(email)
						}), 200
				except Exception as e:
					print("error message", e.message)

			except Exception as e:
				db.session.rollback()
				db.session.close()
				message = ["Error occured with Password Reset Link"]
				return jsonify({
					"message": message,
					"exception": str(e)
				}), 406

@app.route("/view/reset_passwords/all", methods=["GET"])
def view_all_reset_passwords():
    resets = db.session.query(ResetPassword).all()

    if not resets:
        return jsonify({
            "message": "No reset found in the databse"
        }), 412

    else:
        data = []
        for single in resets:
            data.append(single.return_json())

        return jsonify({
            "data": data
        }), 200

@app.route("/view/reset_passwords/<public_id>", methods=["GET"])
def view_single_reset_password(public_id):

    reset = db.session.query(ResetPassword) \
        .filter(ResetPassword.reset_public_id == public_id) \
        .first()

    if not reset:

        return jsonify({
            "message": "The selected reset does not appear to exist"
        }), 404
    else:
        data = [reset.return_json()]

        return jsonify({
            "data": data
        }), 200

@app.route("/resetPassword/<reset_token>", methods=["POST"])
def renew_password(reset_token):
	password = request.json.get("password")

	resets = db.session.query(ResetPassword) \
		.filter(ResetPassword.reset_public_id == reset_token) \
		.first()

	user = db.session.query(User) \
		.filter(User.user_email == resets.email) \
		.first()

	if not resets:
		return jsonify({
			"message": "The selected link does not appear to exist"
		}), 404
	else:
		resets.reset = "True"
		resets.updated_at = datetime.now()
		user.hash_password(password)
		user.updated_at = datetime.now()

		try:
			db.session.commit()
			db.session.close()

			return jsonify({
				"message": "Password reset successfully"
			}), 201

		except Exception as e:
			db.session.rollback()
			db.session.close()

			return jsonify({
				"message": "Error while reseting password",
				"exception": str(e)
			}), 406

@app.route("/email", methods=["POST"])
def send_email():
	email = request.json.get("email")
	message = Mail(
		from_email=app.config['MAIL_ADDRESS'],
		to_emails=email,
		subject='Msacco Testing Platform',
		html_content='<h1>Welcome to Msacco,</h1> '
					 '<p>You are now a registered member</p>'
					 ' <p>Your password is:</p>')
	try:
		sg = SendGridAPIClient(app.config['SENDGRID_API_KEY'])
		response = sg.send(message)
		if response:
			return jsonify({
				"message": "Email has been sent"
			}),200
	except Exception as e:
		print("error message", e.message)
