from flask import Flask, json, jsonify, request, redirect
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
import pymysql
# from flask_caching import Cache
from flasgger import Swagger
from datetime import datetime
from functools import wraps
import jwt

app = Flask(__name__)
CORS(app)

# PostgreSQL
# app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql://postgres:500Horsepower?@188.166.151.87:5432/msaccoapi"
# app.config["SQLALCHEMY_DATABASE_URI"] = "mysql://brian:Admin-17@167.99.50.230:3306/msacco"
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql://root@127.0.0.1:3306/msacco"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] =  False
app.config["JSON_SORT_KEYS"] = False
app.config["JSONIFY_PRETTYPRINT_REGULAR"] = False
app.config['SECRET_KEY'] = 'dt87huJDTG00u890'
# app.config['SENDGRID_API_KEY'] = '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'
app.config['SENDGRID_API_KEY'] = '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'
app.config['MAIL_ADDRESS'] = 'Msacco <noreply@msacco.com>'

db = SQLAlchemy(app)

swagger = Swagger(app)

# def token_required(f):
# 	@wraps(f)
# 	def decorated(*args, **kwargs):
# 		auth_header = request.headers.get('Authorization')
#
# 		if auth_header:
# 			try:
# 				auth_token = auth_header.split(" ")[1]
# 			except IndexError:
# 				return jsonify({
# 					"message": "Bearer token malfunctioned"
# 				}), 401
# 		else:
# 			auth_token = ''
#
# 		if not auth_token:
# 			return jsonify({
# 				"message": "Token is missing"
# 			}),401
# 		try:
# 			data = jwt.decode(auth_token, app.config['SECRET_KEY'], 'utf-8')
# 			# current_user = User.query.filter_by(user_public_id=data['sub']).first()
# 		except Exception as e:
# 			return jsonify({
# 				"message": "Token is invalid",
# 				"error": str(e)
# 			}),401
# 		return f(*args, **kwargs)
# 	return decorated
#

@app.after_request
def append_timestamp_to_json(response):
	data = response.json

	if data:
		access_time = datetime.now()

		data["access_time"] = access_time

		response.set_data(json.dumps(data))

	else:
		pass

	return response

from routes import base_urls, member_urls, user_urls, guarantor_urls, loan_application_urls, auth, notification_urls, loan_url, payments_url, summary_urls
