import requests
import uuid

from flask import jsonify, request
from datetime import datetime
from sqlalchemy.sql import func

from routes.auth_function import token_required

from routes import app, db
from database.user import User
from database.member import Member
from database.guarantor import Guarantor
from database.notification import Notification
from database.loanApplication import LoanApplication
from database.loan import Loan
from database.statement import Statement
from database.interest import Interest
from database.repayment import Repayment

@app.route("/summaries", methods=["GET"])
def summaries():
    user_count = db.session.query(User).count()
    member_count = db.session.query(Member).count()
    loan_application_count = db.session.query(LoanApplication).count()
    loan_count = db.session.query(Loan).count()

    repayment = db.session.query(Repayment).all()  
    repayment_amount = []
    for single in repayment:
        repayment_amount.append(int(single.payment_amount))

    loan = db.session.query(Loan).all()
    total_approved_amount = []
    total_current_balance = []
    
    for single in loan:
        total_approved_amount.append(int(single.approved_amount))
        total_current_balance.append(int(single.current_balance))

    loan_application = db.session.query(LoanApplication).all()
    application_amount = []
    for single in loan_application:
        application_amount.append(int(single.loan_amount_figures))

    return jsonify({
        "number_of_users": user_count,
        "number_of_members": member_count,
        "number_of_loan_applications": loan_application_count,
        "number_of_loans": loan_count,
        "total_loan_repayment": sum(repayment_amount),
        "total_approved_amount": sum(total_approved_amount),
        "total_loan_balance": sum(total_current_balance),
        "total_application": sum(application_amount),
    }),200

@app.route("/summary/<user_id>")
def user_summary(user_id):
    memberDetails = db.session.query(Member).filter(Member.user_public_id == user_id).first()
    if not memberDetails:
        return jsonify({
            "message": "User does not exist"
        }),406

    loan_application_count = db.session.query(LoanApplication).filter(LoanApplication.member_public_id == memberDetails.member_public_id).count()
    
    loan_applications = db.session.query(LoanApplication).filter(LoanApplication.member_public_id == memberDetails.member_public_id).all()
    application_amount = []
    loan_count = ''
    total_approved_amount = []
    total_current_balance = []

    for single in loan_applications:
        application_amount.append(int(single.loan_amount_figures))
    
        loan_count = db.session.query(Loan).filter(Loan.loan_application_public_id == single.loan_application_public_id).count()

        loan = db.session.query(Loan).filter(Loan.loan_application_public_id == single.loan_application_public_id).all()
        for singles in loan:
            total_approved_amount.append(int(singles.approved_amount))
            total_current_balance.append(int(singles.current_balance))
        
    return jsonify({
        "no_of_loan_applications": loan_application_count,
        "number_of_loans": loan_count,
        "total_loan_repayment": sum(total_approved_amount) -sum(total_current_balance),
        "total_approved_amount": sum(total_approved_amount),
        "total_loan_balance": sum(total_current_balance),
        "total_application": sum(application_amount)
    }),200