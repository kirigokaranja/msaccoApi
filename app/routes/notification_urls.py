import requests
import uuid

from flask import jsonify, request
from datetime import datetime

from routes import app, db
from database.notification import Notification

@app.route("/view/notification/<user_id>", methods=["GET"])
def view_notification_per_user(user_id):
    notification = db.session.query(Notification).filter(Notification.recipient_id == user_id, Notification.notification_read == "unread").all()
    notification_count = db.session.query(Notification).filter(Notification.recipient_id == user_id, Notification.notification_read == "unread").count()
    if not notification:
        return jsonify({
            "message": "User does not have Notifications"
        }),404
    else:
        data = []
        for single in notification:
            data.append(single.return_json())
        return jsonify({
                "count": notification_count,
                "data": data
            }),200

@app.route("/update/notification/<notification_id>", methods=["POST"])
def update_notification(notification_id):
    notification = db.session.query(Notification) \
        .filter(Notification.notification_public_id == notification_id) \
        .first()

    if not notification:
        return jsonify({
            "message": "Notification not found"
        }),412
    else:
        status = "read"
        notification.notification_read = status

        try:
            db.session.commit()
            db.session.close()

            message = "Notification has been updated successfully"
            return jsonify({
                "message": message
            }), 201

        except Exception as e:
            db.session.rollback()
            db.session.close()
            message = "Notification has not been updated"
            return jsonify({
                "message": message,
                "exception": str(e)
            }), 406