import requests
import jwt

from flask import jsonify, request
from functools import wraps

from routes import app, db
from database.user import User, BlacklistToken

import pandas as pd
# from sklearn.externals import joblib
import joblib

def decode_auth_token(auth_token):
    try:
        payload = jwt.decode(auth_token, app.config['SECRET_KEY'], 'utf-8')
        is_blacklisted_token = BlacklistToken.check_blacklist(auth_token)
        # print(is_blacklisted_token)
        if is_blacklisted_token:
            # print("token is blacklisted")
            return jsonify({
                "message": "Token blacklisted. Please log in again."
            }),401
        else:
            # print("token is not blacklisted")
            return jsonify({
                "message": "token is not blacklisted"
            })
    except jwt.ExpiredSignatureError:
        return jsonify({
            "message": "Signature expired. Please log in again"
        }),401
    except jwt.InvalidTokenError:
        return jsonify({
            "message": "Invalid token. Please log in again"
        }),401

def token_required(f):
	@wraps(f)
	def decorated(*args, **kwargs):
		auth_header = request.headers.get('Authorization')

		if auth_header:
			try:
				auth_token = auth_header.split(" ")[1]
			except IndexError:
				return jsonify({
					"message": "Bearer token malfunctioned"
				}), 401
		else:
			auth_token = ''

		if not auth_token:
			return jsonify({
				"message": "Token is missing"
			}),401
		try:
			data = decode_auth_token(auth_token)

		except Exception as e:
			return jsonify({
				"message": "Token is invalid",
				"error": str(e)
			}),401
		return f(*args, **kwargs)
	return decorated

def preprocess(data):
    gender = 0
    married = 0
    history = 0
    dependants1 = 0
    dependants2 = 0
    dependants = 0
    education = 0
    employed= 0
    urban = 0
    rural = 0
    if data['gender'] == "Male":
         gender = 1 
    if data['marital_status'] == True:
        married = 1
    if data['dependents'] >= "3":
        dependants = 1
    elif data['dependents'] == "2":
        dependants2 = 1
    elif data['dependents'] == "1":
        dependants1 = 1
    if data['education'] == "Graduated":
        education = 1
    if data['self_employed'] == True:
        employed = 1
    if data['property_area'] == 'Urban':
        urban = 1
    elif data['property_area'] == "Rural":
        rural = 1

    return {
        "Credit_History": history,
        "Gender_Male": gender,
        "Married_Yes": married,
        "Dependents_1": dependants1,
        "Dependents_2": dependants2,
        "Dependents_3+": dependants,
        "Education_Not Graduate": education,
        "Self_Employed_Yes": employed,
        "Property_Area_Semiurban": rural,
        "Property_Area_Urban": urban,
        "Loan_Term_360": 1,
    }

def risk_analysis(data):
    obj = joblib.load("model.pkl")
    model_cols = joblib.load("model_columns.pkl")
    json_data = data
    processed = preprocess(json_data)
    query = pd.DataFrame(processed , index=[0])
    query = query.reindex(columns=model_cols, fill_value=0)
    risk = ""
    prediction = obj.predict(query)
    if prediction[0] == 0.0:
        risk = "Low Risk"
    else:
        risk = "High Risk"
    print(risk)



