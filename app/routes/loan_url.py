import requests
import uuid

from flask import jsonify, request
from datetime import datetime

from routes.auth_function import token_required

from routes import app, db
from database.user import User
from database.member import Member
from database.loanApplication import LoanApplication
from database.loan import Loan
from database.statement import Statement

@app.route("/view/loans/all", methods = ["GET"])
@token_required
def view_all_loans():
    loans = db.session.query(Loan).all()

    if not loans:
        return jsonify({
            "message": "No loans found in the database"
        }), 412

    else:
        data = []
        for single in loans:
            data.append(single.return_json())

        return jsonify({
            "message": "All loans fetched successfully",
			"data": data
		}), 200

@app.route("/view/loan/<user_id>", methods=["GET"])
def view_loan_per_user(user_id):
    user = db.session.query(Member) \
        .filter(Member.user_public_id == user_id) \
        .first()
    application = db.session.query(LoanApplication) \
        .filter(LoanApplication.member_public_id == user.member_public_id) \
        .all()
    if not application:
        return jsonify({
            "message": "User has no loan application"
        }),412
    else:
        data = []
        for single in application:
            # print(single.loan_application_public_id)
                loans = db.session.query(Loan).filter(Loan.loan_application_public_id == single.loan_application_public_id).all()
                for singles in loans:
                    if not singles:
                        return jsonify({
                            "message": "User has no loans"
                        }), 412
                    else:
                        # print(singles.return_json())
                        data.append(singles.return_json())

                        return jsonify({
                            "message": "loan application fetched successfully",
                            "data": data
                        }), 200

## get loan details using loan application id
@app.route("/view/loans/<public_id>", methods=["GET"])
def view_loan_per_public_id(public_id):
    loan = db.session.query(Loan) \
        .filter(Loan.loan_application_public_id == public_id) \
        .first()

    if not loan:
        message = []
        message.append("The selected loan does not appear to exist")

        return jsonify({
            "message": message
        }), 404
    else:
        data = [loan.return_json()]

        return jsonify({
            "data": data
        }), 200




        
        


