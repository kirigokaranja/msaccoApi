import requests
import uuid

from flask import jsonify, request
from datetime import datetime

from routes.auth_function import token_required

from routes import app, db
from database.user import User
from database.member import Member
from database.repayment import Repayment
from database.loanApplication import LoanApplication
from database.loan import Loan
from database.statement import Statement

@app.route("/add/payment/<loan_id>", methods=["POST"])
@token_required
def add_payment(loan_id):
    loan = db.session.query(Loan) \
        .filter(Loan.loan_public_id == loan_id) \
        .first()
    if not loan:
        return jsonify({'message': 'Loan Not Found'}), 412
    else:
        amount = request.json.get("amount")
        payment = Repayment(
            payment_public_id = str(uuid.uuid4()),
            loan_id = loan_id,
            payment_date = datetime.now(),
            payment_amount = amount,
            payment_type = "Cash Payment",
            created_at=datetime.now(),
            updated_at=datetime.now()
        )
        db.session.add(payment)
        statement = Statement(
            statement_public_id=str(uuid.uuid4()),
            loan_id=loan_id,
            statement_type="Loan Repayment",
            statement_date=datetime.now(),
            debit=amount,
            credit = "0",
            created_at=datetime.now(),
            updated_at=datetime.now()
        )
        db.session.add(statement)

        balance = int(loan.current_balance) - int(amount)
        loan.current_balance = balance
        loan.updated_at = datetime.now()

        try:
            db.session.commit()
            db.session.close()

            message = ["Loan Repayment has been updated successfully"]
            return jsonify({
                "message": message
            }), 201

        except Exception as e:
            db.session.rollback()
            db.session.close()
            message = ["Loan Repayment has not been updated"]
            return jsonify({
                "message": message,
                "exception": str(e)
            }), 406

@app.route("/statements", methods=["GET"])
def get_statements():
    statement = db.session.query(Statement).all()

    if not statement:
        return jsonify({
            "message": "No Statements found in the database"
        }), 412

    else:
        data = []
        for single in statement:
            data.append(single.return_json())

            credit = [int(x) for x in [single.credit]]
            debit = [int(x) for x in [single.debit]]
            

        return jsonify({
            "message": "All Statements fetched successfully",
			"data": data, 
            "total_credit": sum(credit),
            "total_debit": sum(debit)
		}), 200

## get loan and member details from loan application id
@app.route("/view/members/statement/<public_id>", methods=["GET"])
def view_statement_member(public_id):
    details = db.session.query(Loan).filter(Loan.loan_application_public_id == public_id).first()

    statement_details = db.session.query(Statement).filter(Statement.loan_id == details.loan_public_id).all()

    application = db.session.query(LoanApplication).filter(LoanApplication.loan_application_public_id == public_id).first()

    user = db.session.query(Member).filter(Member.member_public_id == application.member_public_id).first()
    if not statement_details:
        return jsonify({
            "message": "No loan found"
        }),404
    else:
        message = "Loan information fetched successfully"
        members = {
            "member_id" : user.member_id,
            "member_names" : user.firstname + " " + user.lastname,
            "member_shares": user.member_shares
        }

        data = []
        for single in statement_details:
            credit = [int(x) for x in [single.credit]]
            debit = [int(x) for x in [single.debit]]
            data.append(single.return_json())
        return jsonify({
                "mesage": message,
                "balance": details.current_balance,
                "total_credit": sum(credit),
                "total_debit": sum(debit),
                "member": members,
                "data": data
            }),200