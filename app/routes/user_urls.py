import requests
import uuid

from flask import jsonify, request
from datetime import datetime

from routes import app, db
from database.user import User


@app.route("/user/add", methods=["POST"])
def add_new_user():
    user_id = str(uuid.uuid4())
    now = datetime.now()
    user_password = request.json.get("password")
    user = User(
        user_public_id=user_id,
        user_email=request.json.get("email"),
        firstname=request.json.get("first_name"),
        lastname=request.json.get("last_name"),
        user_status="Active",
        user_category=1,
        created_at=now,
        updated_at=now
    )
    user.hash_password(user_password)
    db.session.add(user)

    try:
        db.session.commit()
        db.session.close()

        message = ["User has been added successfully"]
        return jsonify({
            "message": message
        }), 201

    except Exception as e:
        db.session.rollback()
        db.session.close()
        message = ["User has not been added"]
        return jsonify({
            "message": message,
            "exception": str(e)
        }), 406


@app.route("/view/users/all", methods=["GET"])
def view_all_users():
    users = db.session.query(User).all()

    if not users:
        message = ["No user found in the databse"]
        return jsonify({
            "message": message
        }), 412

    else:
        data = []
        for single in users:
            data.append(single.return_json())

        return jsonify({
            "data": data
        }), 200


@app.route("/view/user/<public_id>", methods=["GET"])
def view_single_user(public_id):
    """
	Return details of a single user
"""
    user = db.session.query(User) \
        .filter(User.user_public_id == public_id) \
        .first()

    if not user:
        message = ["The selected user does not appear to exist"]

        return jsonify({
            "message": message
        }), 404
    else:
        data = [user.return_json()]

        return jsonify({
            "data": data
        }), 200


@app.route("/update/user/<public_id>", methods=["POST"])
def update_single_user(public_id):
    user = db.session.query(User) \
        .filter(User.user_public_id == public_id) \
        .first()

    if not user:
        return jsonify({'message': 'User Not Found'}), 412
    else:
        user_password = request.json.get("password"),
        user_status = request.json.get("user_status"),
        firstname = request.json.get("first_name"),
        lastname = request.json.get("last_name"),

        user.user_password = user_password
        user.user_status = user_status
        user.firstname = firstname
        user.lastname = lastname
        user.updated_at = datetime.now()

        try:
            db.session.commit()
            db.session.close()

            message = ["User has been updated successfully"]
            return jsonify({
                "message": message
            }), 201

        except Exception as e:
            db.session.rollback()
            db.session.close()
            message = ["User has not been updated"]
            return jsonify({
                "message": message,
                "exception": str(e)
            }), 406
