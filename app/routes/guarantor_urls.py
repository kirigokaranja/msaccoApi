import requests
import uuid

from flask import jsonify, request
from datetime import datetime

from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

from routes import app, db
from database.guarantor import Guarantor
from database.member import Member
from database.user import User
from database.loanApplication import LoanApplication

@app.route("/view/guarantors/all", methods = ["GET"])
def view_all_guarantors():
    guarantors = db.session.query(Guarantor).all()

    if not guarantors:
        message = ["No guarantor found in the database"]
        return jsonify({
            "message": message
        }), 412

    else:
        data = []
        for single in guarantors:
            data.append(single.return_json())

        return jsonify({
            "data": data
        }), 200

@app.route("/view/guarantor/<public_id>", methods = ["GET"])
def view_single_guarantor(public_id):
    """
    Return details of a single user
    """
    guarantor = db.session.query(Guarantor)\
                     .filter(Guarantor.guarantor_public_id == public_id)\
                     .first()

    if not guarantor:
        message = []
        message.append("The selected guarantor does not appear to exist")

        return jsonify({
            "message": message
        }), 404
    else:
        data = [guarantor.return_json()]

        return jsonify({
            "data": data
        }), 200

@app.route("/guarantor/recommendation/<guarantor_id>", methods=["POST"])
def guarantor_recommendation(guarantor_id):
    guarantee = db.session.query(Guarantor) \
        .filter(Guarantor.guarantor_public_id == guarantor_id) \
        .first()
    application = db.session.query(LoanApplication) \
        .filter(LoanApplication.loan_application_public_id == guarantee.loan_application_public_id) \
        .first()

    if not application:
        return jsonify({'message': 'Loan Application not found'}), 412
    if not guarantee:
        return jsonify({'message': 'Guarantor Not Found'}), 412
    else:
        if guarantee.recommendation != "pending":
            return jsonify({
                "message": "Loan decision has already been made"
            }),401

        user = db.session.query(Member) \
            .filter(Member.member_public_id == application.member_public_id) \
            .first()

        g_member = db.session.query(Member) \
            .filter(Member.member_public_id == guarantee.member_public_id) \
            .first()

        user_det = db.session.query(User) \
            .filter(User.user_public_id == user.user_public_id) \
            .first()

        recommendation = request.json.get("recommendation")
        email = request.json.get("email")
        email = user_det.user_email
        guarantee.recommendation = recommendation
        guarantee.updated_at = datetime.now()
        recommendation_message = ()

        if recommendation == "Rejected":
            recommendation_message = "{0} has rejected been your guarantor. You need to review and apply for a new loan as your loan has been rejected".format(g_member.firstname +" "+ g_member.lastname)
            loan_status = "Rejected"
            reject_reason = "Guarantor rejected loan"

            application.loan_status = loan_status
            application.reject_reason = reject_reason
            application.updated_at = datetime.now()
        if recommendation == "Accepted":
            recommendation_message = "A guarantor has accepted your loan application"

        member_email = Mail(
            from_email=app.config['MAIL_ADDRESS'],
            to_emails=email,
            subject='Msacco Loan Application Guarantor Result',
            html_content='<h1>Msacco Loan Application,</h1> '
                         '<p>Your loan has been reviewed by your guarantor and they have made their decision</p>'
                         ' <p>{0}</p>'.format(recommendation_message))

        sg = SendGridAPIClient(app.config['SENDGRID_API_KEY'])
        response = sg.send(member_email)

        try:
            if response:
                db.session.commit()
                db.session.close()

                message = "Your decision has been added successfully"
                return jsonify({
                    "message": message
                }), 200
            else:
                return jsonify({
                    "message": "Issues sending the email"
                }),403

        except Exception as e:
            db.session.rollback()
            db.session.close()
            message = "Error while adding the decision"
            return jsonify({
                "message": message,
                "exception": str(e)
            }), 406

@app.route("/view/memberGuarantors/all", methods = ["GET"])
def view_all_member_guarantors():
    guarantors = db.session.query(Guarantor).all()

    if not guarantors:
        message = ["No guarantor found in the database"]
        return jsonify({
            "message": message
        }), 412

    else:
        data = []
        for single in guarantors:
            data.append(single.return_member_guarantor_details_json())

        return jsonify({
            "data": data
        }), 200

@app.route("/view/memberGuarantors/<public_id>", methods = ["GET"])
def view__member_guarantors(public_id):
    guarantors = db.session.query(Guarantor).filter(Guarantor.user_public_id == public_id).all()

    if not guarantors:
        message = ["No guarantor found in the database"]
        return jsonify({
            "message": message
        }), 412

    else:
        data = []
        for single in guarantors:
            # print(single.loan_application_public_id)
            loans = db.session.query(LoanApplication).filter(LoanApplication.loan_application_public_id == single.loan_application_public_id).all()
            # print(loans)
            for singles in loans:
                print("singles.return_json()")
            data.append(singles.return_json())

        return jsonify({
			"data": data
		}), 200

@app.route("/view/memberGuarantor/<public_id>", methods = ["GET"])
def view_single_member_guarantor(public_id):
    """
    Return details of a single user
    """
    guarantor = db.session.query(Guarantor)\
                     .filter(Guarantor.guarantor_public_id == public_id)\
                     .first()

    if not guarantor:
        message = []
        message.append("The selected guarantor does not appear to exist")

        return jsonify({
            "message": message
        }), 404
    else:
        data = [guarantor.return_member_guarantor_details_json()]

        return jsonify({
            "data": data
        }), 200

@app.route("/view/loanGuarantor/<public_id>", methods = ["GET"])
def view_single_loan_guarantor(public_id):
    guarantor = db.session.query(Guarantor).filter(Guarantor.user_public_id == public_id).filter(Guarantor.recommendation == 'pending').all()

    if not guarantor:
        message = []
        message.append("The selected guarantor does not appear to exist")

        return jsonify({
            "message": message
        }), 404
    else:
        data = []
        for single in guarantor:
            data.append(single.return_json())

        return jsonify({
            "data": data
        }), 200