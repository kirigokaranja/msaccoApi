import requests
import uuid
import os.path

from flask import jsonify, request
from datetime import datetime
import pandas as pd
from sklearn.externals import joblib

from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from routes.auth_function import token_required
from routes.auth_function import risk_analysis

from routes import app, db
from database.user import User
from database.member import Member
from database.guarantor import Guarantor
from database.notification import Notification
from database.loanApplication import LoanApplication
from database.loan import Loan
from database.statement import Statement

scriptpath = os.path.dirname(__file__)
model = os.path.join(scriptpath, "model.pkl")
cols = os.path.join(scriptpath, "model_columns.pkl")
obj = joblib.load(model)
model_columns = joblib.load(cols)

def preprocess(model_data):
    if obj:
        query = pd.DataFrame(model_data, index=[0])
        categorical_subset = query[['Term', 'Years in current job', 'Home Ownership', 'Purpose']]
        categorical_subset = pd.get_dummies(categorical_subset)
        query.drop(labels=['Term', 'Years in current job', 'Home Ownership', 'Purpose'], axis=1, inplace=True)
        query = pd.concat([query, categorical_subset], axis=1)
        query = query.reindex(columns=model_columns, fill_value=0)
        prediction = list(obj.predict(query))
        if prediction[0] == 1:
            risk = "Low Risk"
        else:
            risk = "High Risk"
        return risk
    else:
        return jsonify({
            "message": "No Trained Model Found"
        }), 403

@app.route("/loanApplication/add/<user_public_id>", methods = ["POST"])
@token_required
def add_new_loan_application(user_public_id):

    loan_app_id = str(uuid.uuid4())
    guarantor_id1 = str(uuid.uuid4())
    guarantor_id2 = str(uuid.uuid4())
    notification_id = str(uuid.uuid4())

    user_public_id1 = request.json.get("user_public_id1")
    user_public_id2 = request.json.get("user_public_id2")

    member = db.session.query(Member) \
        .filter(Member.user_public_id == user_public_id) \
        .first()
    member_id = member.member_public_id
    print(member.return_json())

    memberUser = db.session.query(User) \
        .filter(User.user_public_id == user_public_id) \
        .first()
    memail = memberUser.user_email

    user1 = db.session.query(User) \
        .filter(User.user_public_id == user_public_id1) \
        .first()
    email1 = user1.user_email

    member1 = db.session.query(Member) \
        .filter(Member.user_public_id == user_public_id1) \
        .first()

    user2 = db.session.query(User) \
        .filter(User.user_public_id == user_public_id2) \
        .first()
    email2 = user2.user_email

    member2 = db.session.query(Member) \
        .filter(Member.user_public_id == user_public_id2) \
        .first()

    if request.json.get("no_of_installements") >= 180:
        term = "Long Term"
    else:
        term = "Short Term"

    model_data = {
        "Current Loan Amount": request.json.get("loan_amount_figures"),
        "Term": term,
        "Credit Score": member.credit_score,
        "Annual Income": member.annual_income,
        "Monthly Debt": request.json.get("monthly_installements_figures"),
        "Years of Credit History": member.years_history,
        "Number of Credit Problems": member.credit_problems,
        "Years in current job": member.years_job,
        "Home Ownership": member.home_ownership,
        "Purpose": request.json.get("loan_purpose"),
        "Tax Liens": member.tax_liens,
        "Maximum Open Credit": member.member_shares
    }
    risk_class = preprocess(model_data)

    application = LoanApplication(
        loan_application_public_id = loan_app_id,
        member_public_id = member_id,
        loan_amount_figures = request.json.get("loan_amount_figures"),
        loan_amount_words = request.json.get("loan_amount_words"),
        no_of_installements = request.json.get("no_of_installements"),
        monthly_installements_figures = request.json.get("monthly_installements_figures"),
        monthly_installements_words = request.json.get("monthly_installements_words"),
        loan_purpose = request.json.get("loan_purpose"),
        loan_status = 15,
        risk_class = risk_class,
        guarantor_public_id1 = guarantor_id1,
        guarantor_public_id2 = guarantor_id2,
        guarantor_public_id3 = "null",
        application_date = datetime.now(),
        created_at = datetime.now(),
        updated_at = datetime.now()
    )
    db.session.add(application)

    guarantor1 = Guarantor(
        guarantor_public_id = guarantor_id1,
        user_public_id = user_public_id1,
        loan_application_public_id = loan_app_id,
        member_public_id = member1.member_public_id,
        recommendation = "pending",
        amount_guaranteed = request.json.get("amount_guaranteed1"),
        other_loans_guaranteed = request.json.get("other_loans_guaranteed1"),
        created_at = datetime.now(),
        updated_at = datetime.now()
    )
    db.session.add(guarantor1)

    guarantor2 = Guarantor(
        guarantor_public_id = guarantor_id2,
        user_public_id = user_public_id2,
        member_public_id=member2.member_public_id,
        loan_application_public_id = loan_app_id,
        recommendation = "pending",
        amount_guaranteed = request.json.get("amount_guaranteed2"),
        other_loans_guaranteed = request.json.get("other_loans_guaranteed2"),
        created_at = datetime.now(),
        updated_at = datetime.now()
    )
    db.session.add(guarantor2)

    notification = Notification(
        notification_public_id = notification_id,
        message = "There is a new loan Application",
        recipient_id = user_public_id2,
        notification_status = "Loan Application",
        notification_read = "unread",
        notification_date = datetime.now(),
        created_at=datetime.now(),
        updated_at=datetime.now()
    )
    db.session.add(notification)

    notification1 = Notification(
        notification_public_id=str(uuid.uuid4()),
        message="You have a new loan as a guarantee",
        recipient_id=user_public_id1,
        notification_status="Loan Application",
        notification_read="unread",
        notification_date=datetime.now(),
        created_at=datetime.now(),
        updated_at=datetime.now()
    )
    db.session.add(notification1)

    notification2 = Notification(
        notification_public_id=str(uuid.uuid4()),
        message="You have a new loan as a guarantee",
        recipient_id=user_public_id2,
        notification_status="Loan Application",
        notification_read="unread",
        notification_date=datetime.now(),
        created_at=datetime.now(),
        updated_at=datetime.now()
    )
    db.session.add(notification2)

    if email1 == email2:
        return jsonify({
            "message": "The guarantors cannot be the same person"
        }),403
    else:
        member_email = Mail(
            from_email=app.config['MAIL_ADDRESS'],
            to_emails= memail,
            subject='Msacco Loan Application Received',
            html_content='<h1>Msacco Loan Application,</h1> '
                         '<p>Thank you for using our services</p>'
                         ' <p>Your loan has been received, we will be with you soon</p>')

        guarantor_email = Mail(
            from_email=app.config['MAIL_ADDRESS'],
            to_emails=email1,
            subject='Msacco Loan Application Received',
            html_content='<h1>Msacco Loan Application,</h1> '
                         '<p>You have been selected as a guarantor by {}</p>'
                         ' <p>Kindly log in to the system to Accept/Reject Loan application</p>'
                        'http://localhost:8080'.format(memberUser.firstname +" "+ memberUser.lastname))

        guarantor_email2 = Mail(
            from_email=app.config['MAIL_ADDRESS'],
            to_emails=email2,
            subject='Msacco Loan Application Received',
            html_content='<h1>Msacco Loan Application,</h1> '
                         '<p>You have been selected as a guarantor by {}</p>'
                         ' <p>Kindly log in to the system to Accept/Reject Loan application</p>'
                            'http://localhost:8080'.format(
                memberUser.firstname + " " + memberUser.lastname))

        try:
            db.session.commit()
            db.session.close()

            sg = SendGridAPIClient(app.config['SENDGRID_API_KEY'])
            response = sg.send(member_email)
            response1 = sg.send(guarantor_email)
            response2 = sg.send(guarantor_email2)

            if response and response1 and response2:

                return jsonify({
                    "message": "Loan Application has been added successfully"
                }), 201

        except Exception as e:
            db.session.rollback()
            db.session.close()
            return jsonify({
                "message": "Loan Application has not been added",
                "exception": str(e),
                "application": str(application),
                "guarantor1": str(guarantor1),
                "guarantor2": str(guarantor2)
            }), 406

@app.route("/view/loanApplications/all", methods = ["GET"])
@token_required
def view_all_loan_applications():
    applications = db.session.query(LoanApplication).all()

    if not applications:
        return jsonify({
            "message": "No applications found in the database"
        }), 412

    else:
        data = []
        for single in applications:
            data.append(single.return_loan_member_json())

        return jsonify({
            "message": "All loan applications fetched successfully",
			"data": data
		}), 200

@app.route("/view/loanApplications/pending", methods = ["GET"])
def view_pending_loan_applications():
    applications = db.session.query(LoanApplication).filter(LoanApplication.loan_status == '15').all()

    if not applications:
        return jsonify({
            "message": "No applications found in the database"
        }), 412

    else:
        data = []
        for single in applications:
            data.append(single.return_loan_member_json())
        for allData in data:
            guarantor = []
            for individual in allData['guarantors']:
                recommendation = individual['recommendation']
                guarantor.append(recommendation)
            if guarantor[0] == guarantor[1]:
                allData['status'] = guarantor[0]
            elif guarantor[0] == 'pending':
                allData['status'] = guarantor[0]


        return jsonify({
            "message": "All pending loan applications fetched successfully",
			"data": data
		}), 200

@app.route("/view/loanApplications/rejected", methods = ["GET"])
def view_rejected_loan_applications():
    applications = db.session.query(LoanApplication).filter(LoanApplication.loan_status == 'Rejected').all()

    if not applications:
        return jsonify({
            "message": "No applications found in the database"
        }), 412

    else:
        data = []
        for single in applications:
            data.append(single.return_loan_member_json())

        return jsonify({
            "message": "Rejected loan applications fetched successfully",
			"data": data
		}), 200

@app.route("/view/rejectedLoanApplication/<user_id>", methods = ["GET"])
def view_single_rejected_loan_application(user_id):
    member = db.session.query(Member).filter(Member.user_public_id == user_id).first()
    application = db.session.query(LoanApplication).filter(LoanApplication.member_public_id == member.member_public_id).filter(LoanApplication.loan_status == 'Rejected').all()
    if not application:
        return jsonify({
            "message": "The selected application does not appear to exist"
        }),404
    else:
        data = []
        for single in application:
            data.append(single.return_loan_member_json())
        return jsonify({
            "message": "rejected loan application fetched successfully",
            "data": data
        }), 200

@app.route("/view/loanApplication/<user_id>", methods = ["GET"])
@token_required
def view_single_loan_application(user_id):
    member = db.session.query(Member).filter(Member.user_public_id == user_id).first()
    application = db.session.query(LoanApplication).filter(LoanApplication.member_public_id == member.member_public_id).all()
    if not application:
        return jsonify({
            "message": "The selected application does not appear to exist"
        }),404
    else:
        data = []
        for single in application:
            data.append(single.return_loan_member_json())
        return jsonify({
            "message": "loan application fetched successfully",
            "data": data
        }), 200

@app.route("/view/specificLoanApplication/<loan_id>", methods = ["GET"])
@token_required
def view_specific_loan_application(loan_id):
    application = db.session.query(LoanApplication).filter(LoanApplication.loan_application_public_id == loan_id).first()
    if not application:
        return jsonify({
            "message": "The selected application does not appear to exist"
        }),404
    else:
        data = application.return_loan_member_json()
        return jsonify({
            "message": "loan application fetched successfully",
            "data": data
        }), 200

@app.route("/update/loanApplication/<public_id>", methods = ["POST"])
@token_required
def update_single_application(public_id):
    
    application = db.session.query(LoanApplication)\
					 .filter(LoanApplication.loan_application_public_id == public_id)\
					 .first()

    if not application:
        return jsonify({'message': 'Loan Application not found'}), 412
    else:
        loan_amount_figures = request.json.get("loan_amount_figures"),
        loan_amount_words = request.json.get("loan_amount_words"),
        no_of_installements = request.json.get("no_of_installements"),
        monthly_installements_figures = request.json.get("monthly_installements_figures"),
        monthly_installements_words = request.json.get("monthly_installements_words"),
        loan_purpose = request.json.get("loan_purpose")
        
        application.loan_amount_figures = loan_amount_figures
        application.loan_amount_words = loan_amount_words
        application.no_of_installements = no_of_installements
        application.monthly_installements_figures = monthly_installements_figures
        application.monthly_installements_words = monthly_installements_words
        application.loan_purpose = loan_purpose
        application.updated_at = datetime.now()

        try:
            db.session.commit()
            db.session.close()

            message = ["Loan Application has been updated successfully"]
            return jsonify({
                "message": message
            }), 201

        except Exception as e:
            db.session.rollback()
            db.session.close()
            message = ["Loan Application has not been updated"]
            return jsonify({
                "message": message,
                "exception": str(e)
            }), 406

@app.route("/loanApplication/decision/<public_id>", methods=["POST"])
@token_required
def loan_decision(public_id):
    application = db.session.query(LoanApplication) \
        .filter(LoanApplication.loan_application_public_id == public_id) \
        .first()

    if not application:
        return jsonify({'message': 'Loan Application not found'}), 412
    else:
        if application.loan_status is "Accepted":
            return jsonify({
                "message": "The decision for the loan application has already been made"
            }),403
        elif application.loan_status is "Rejected":
            return jsonify({
                "message": "The decision for the loan application has already been made"
            }), 403
        else:

            user = db.session.query(Member) \
                .filter(Member.member_public_id == application.member_public_id) \
                .first()

            user_det = db.session.query(User) \
                .filter(User.user_public_id == user.user_public_id) \
                .first()

            loan_status = request.json.get("loan_status")
            reject_reason = request.json.get("reject_reason")
            email = user_det.user_email

            application.loan_status = loan_status
            application.reject_reason = reject_reason
            application.updated_at = datetime.now()

            notification = Notification(
                notification_public_id=str(uuid.uuid4()),
                message="Loan Application Decision has been made",
                recipient_id=user.user_public_id,
                notification_status="Loan Application Decision",
                notification_read="unread",
                notification_date=datetime.now(),
                created_at=datetime.now(),
                updated_at=datetime.now()
            )
            db.session.add(notification)

            if loan_status == 'Accepted':
                loan_id = str(uuid.uuid4())
                new_loan = Loan(
                    loan_public_id=loan_id,
                    loan_application_public_id=public_id,
                    approved_date=datetime.now(),
                    approved_amount=application.loan_amount_figures,
                    current_balance=application.loan_amount_figures,
                    created_at=datetime.now(),
                    updated_at=datetime.now()
                )
                db.session.add(new_loan)

                statement = Statement(
                    statement_public_id=str(uuid.uuid4()),
                    loan_id=loan_id,
                    statement_type="Initial Loan Amount",
                    statement_date=datetime.now(),
                    credit=application.loan_amount_figures,
                    debit = "0",
                    created_at=datetime.now(),
                    updated_at=datetime.now()
                )
                db.session.add(statement)

            try:
                db.session.commit()
                db.session.close()

                member_email = Mail(
                    from_email=app.config['MAIL_ADDRESS'],
                    to_emails=email,
                    subject='Msacco Loan Application Result',
                    html_content='<h1>Msacco Loan Application,</h1> '
                                 '<p>Your loan has been reviewed by the Msacco officials</p>'
                                 ' <p>Your loan has been {0}</p>'.format(loan_status))

                sg = SendGridAPIClient(app.config['SENDGRID_API_KEY'])
                response = sg.send(member_email)

                if response:
                    message = ["You have submitted the decision for the loan application"]
                    return jsonify({
                        "message": message
                    }), 201

            except Exception as e:
                db.session.rollback()
                db.session.close()
                message = ["Loan Application has not been updated"]
                return jsonify({
                    "message": message,
                    "exception": str(e)
                }), 406
